# Introduction - App
The project is containerized with Docker and it will run in a Docker Container. To build and run it, just simply run the command bellow: 

`sh ./setup.sh`

If setup.sh file is not executable, run the command below:
`chmod +x setup.sh`

This will build and run the container and expose it on the port 3000, being available in the address: 
[localhost:3001](localhost:3001 "localhost:3001")

# Solution
I've created a simple app to **list**, **edit** and **add** new animals. 

# Technologies
To create this project I've chosen to use **NestJS** with **TypeORM** and **Postgres**, using **Jest** to test the application.
