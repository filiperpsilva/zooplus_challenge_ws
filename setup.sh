docker run --name zoo-postgres -e POSTGRES_PASSWORD=123456789 -p 5432:5432 -d postgres

sleep 5

docker exec -it zoo-postgres psql -U postgres -c 'create DATABASE zooplus_challenge;'
 
docker build -t zooplus_challenge_ws .

export POSTGRES_HOST=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' zoo-postgres)

docker run -p 3001:3001 -e POSTGRES_HOST=$POSTGRES_HOST -e POSTGRES_PASSWORD=123456789 -d zooplus_challenge_ws