## Instructions
This is a small exercise to test your skills in:
- Backend development (You can choose any language)
- Web services (REST)
- Web apps. Use an MVC framework (Vue, Angular, React)
- Database. Use any solution (Local JSON file, MongoDB, Postgres...)

For giving you a bit of context, in Zoobrain our current stack is:
- Vue 2 with Vuetify, Jest, Cypress & Cucumber for the tests
- Scala with Akka HTTP
- Postgres
- AWS, K8s, Docker

But please, feel free to chose any language, tool or framework that you want in order to complete the code challenge.

The purpose is to implement a simple web application and an REST API to consume. The web application will make use of the API to retrieve and show information in a proper but simple way. The data will be based on a JSON provided in the exercise related with animals.
Your application will be able to list the animals and to create a new, or update an animal using the API.

#### _Web App_
- Query the API to retrieve a list of animals based on the JSON provided in this exercise and display the data.
- Query the API to show the details of an animal.

This is the structure of an animal:

```json

{

"id": "74fc362a10d1f0f8ba827e82280cd2ec",
"name": "Example animal",
"type": "Cat",
"breed": "Persian",
"gender": "Female",
"vaccinated": true,
"lastVisit": "2021-01-28T04:03:02.216Z",
"lastUpdate": "2021-01-28T04:03:02.216Z"
}

```
 

#### _API_
- Build the data from your database using the template provided in the exercise.
- Manage the data persistence to allow new data to be introduced.
- Allow to retrieve the items saved on the persistence layer.
- Allow to introduce new items and save them on the persistence layer.

**It is important that the code is maintainable, well structured and well tested using your preferred testing framework**

## Things to be considered before starting
- How you structure your app
- No complete DB product is needed. Memory persistence to mock the DB is enough.
- Which patterns you use
- How you define the API
- Testing
- How to build the app

## How to proceed
- Please use whatever repository you prefer, but make sure it's public so we can check the solution.
- Implement the problem above in your own repository.
- Please include a solution.md file with all the relevant information for the result, such as assumptions or how to build it and run the application.
- Once you are finish send us the link to the repo and we will check it.

  

## Important
- You can make your own assumptions to solve the problem. Please indicate them in the solution.md.
- If you have any doubts about the problem or something that you would like us to clarify, please feel free to contact us.
- Even though we want a full working app we are going to focus more into detail in the frontend (Design, components, style).

## Must have
- The application must run and work correctly.
- The application must have unit tests to prove that it works.
- E2E test.
- The application must include all kind of necessary tests to prove that it works (You will have to defend your decission in the tech interview)
- Containerisation

## Nice to have
- Design at high level how you would deploy the app in a kubernetes environment (Which resources you would need).

## Bonus points
- Include behavioural tests, BDD

Thanks your for time!