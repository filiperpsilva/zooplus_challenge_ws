import { Module } from '@nestjs/common';
import { AnimalController } from './animal/animal.controller';
import { AnimalModule } from './animal/animal.module';
import { AnimalProviders } from './animal/animal.provider';
import { AnimalService } from './animal/animal.service';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [DatabaseModule, AnimalModule],
  controllers: [AppController, AnimalController],
  providers: [AppService, AnimalService, ...AnimalProviders],
})
export class AppModule {}
