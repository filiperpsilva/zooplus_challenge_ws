import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { AnimalProviders } from './animal.provider';
import { AnimalService } from './animal.service';

@Module({
  imports: [DatabaseModule],
  providers: [...AnimalProviders, AnimalService],
})
export class AnimalModule {}
