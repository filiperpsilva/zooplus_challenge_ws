import { Test, TestingModule } from '@nestjs/testing';
import { AnimalService } from '../animal.service';
import { Repository } from 'typeorm';
import { Animal } from '../animal.entity';

describe('AnimalService.findById', () => {
    let animalService: AnimalService;
    let animalRepository: Repository<Animal>;
  
    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        providers: [
          AnimalService,
          {
            provide: 'ANIMAL_REPOSITORY',
            useClass: Repository,
          },
        ],
      }).compile();
  
      animalService = moduleRef.get<AnimalService>(AnimalService);
      animalRepository = moduleRef.get('ANIMAL_REPOSITORY');
    });
  
    describe('findById', () => {
      const animalId = '1';
  
      it('should return the animal when found', async () => {
        const animal = new Animal();
        animal.id = animalId;
        animal.name = 'Fido';
  
        jest.spyOn(animalRepository, 'findOne').mockResolvedValue(animal);
  
        const result = await animalService.findById(animalId);
  
        expect(result).toBe(animal);
      });
  
      it('should throw an error when animal is not found', async () => {
        jest.spyOn(animalRepository, 'findOne').mockResolvedValue(undefined);
  
        await expect(animalService.findById(animalId)).rejects.toThrow(
          `Animal with id ${animalId} not found`,
        );
      });
    });
  });
  