import { AnimalService } from '../animal.service';
import { Animal } from '../animal.entity';
import { UpdateAnimalDto } from '../animal.dto';

describe('AnimalService.updateAnimal', () => {
    
  const mockAnimalRepository = {
    findOne: jest.fn(),
    save: jest.fn(),
  };

  const animalsService = new AnimalService(mockAnimalRepository as any);

  it('should throw an error when animal is not found', async () => {
    const mockAnimalId = '123';
    const mockUpdateAnimalDto: UpdateAnimalDto = {
      id: '123',
      name: 'Updated Animal',
      breed: 'Updated Breed',
      gender: 'Updated Gender',
      type: 'Updated Type',
      vaccinated: true,
    };
    mockAnimalRepository.findOne.mockResolvedValue(undefined);

    await expect(
      animalsService.updateAnimal(mockAnimalId, mockUpdateAnimalDto),
    ).rejects.toThrow(`Animal with ID ${mockAnimalId} not found`);

    expect(mockAnimalRepository.findOne).toHaveBeenCalledWith({
      where: { id: mockAnimalId },
    });
    expect(mockAnimalRepository.save).not.toHaveBeenCalled();
  });

  it('should update and return the animal', async () => {
    const mockAnimalId = '123';
    const mockUpdateAnimalDto: UpdateAnimalDto = {
      id: '123',
      name: 'Updated Animal',
      breed: 'Updated Breed',
      gender: 'Updated Gender',
      type: 'Updated Type',
      vaccinated: true,
    };
    const mockAnimal = new Animal();
    mockAnimal.id = '123';
    mockAnimal.name = 'Original Animal';
    mockAnimal.breed = 'Original Breed';
    mockAnimal.gender = 'Original Gender';
    mockAnimal.type = 'Original Type';
    mockAnimal.vaccinated = false;
    mockAnimalRepository.findOne.mockResolvedValue(mockAnimal);
    mockAnimalRepository.save.mockResolvedValue({
      ...mockAnimal,
      ...mockUpdateAnimalDto,
    });

    const result = await animalsService.updateAnimal(
      mockAnimalId,
      mockUpdateAnimalDto,
    );

    expect(mockAnimalRepository.findOne).toHaveBeenCalledWith({
      where: { id: mockAnimalId },
    });
    expect(mockAnimalRepository.save).toHaveBeenCalledWith({
      ...mockAnimal,
      ...mockUpdateAnimalDto,
    });
    expect(result).toEqual({ ...mockAnimal, ...mockUpdateAnimalDto });
  });

  it('should throw an error when save fails', async () => {
    const mockAnimalId = '123';
    const mockUpdateAnimalDto: UpdateAnimalDto = {
      id: '123',
      name: 'Updated Animal',
      breed: 'Updated Breed',
      gender: 'Updated Gender',
      type: 'Updated Type',
      vaccinated: true,
    };
    const mockAnimal = new Animal();
    mockAnimal.id = '123';
    mockAnimal.name = 'Original Animal';
    mockAnimal.breed = 'Original Breed';
    mockAnimal.gender = 'Original Gender';
    mockAnimal.type = 'Original Type';
    mockAnimal.vaccinated = false;
    mockAnimalRepository.findOne.mockResolvedValue(mockAnimal);
    mockAnimalRepository.save.mockRejectedValue(new Error('Save error'));

    await expect(
      animalsService.updateAnimal(mockAnimalId, mockUpdateAnimalDto),
    ).rejects.toThrow(
      `Failed to update animal with ID ${mockAnimalId}: Save error`,
    );

    expect(mockAnimalRepository.findOne).toHaveBeenCalledWith({
      where: { id: mockAnimalId },
    });
    expect(mockAnimalRepository.save).toHaveBeenCalledWith({
      ...mockAnimal,
      ...mockUpdateAnimalDto,
    });
  });
});
