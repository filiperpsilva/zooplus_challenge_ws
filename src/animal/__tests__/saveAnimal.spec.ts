import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { AnimalService } from '../animal.service';
import { Animal } from '../animal.entity';
import { CreateAnimalDto } from '../animal.dto';


describe('AnimalService.saveAnimal', () => {
  let animalService: AnimalService;
  let animalRepository: Repository<Animal>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AnimalService,
        {
          provide: 'ANIMAL_REPOSITORY',
          useClass: Repository,
        },
      ],
    }).compile();

    animalService = module.get<AnimalService>(AnimalService);
    animalRepository = module.get<Repository<Animal>>('ANIMAL_REPOSITORY');
  });

  describe('saveAnimal', () => {
    it('should save a new animal', async () => {
      
      const createAnimalDto: CreateAnimalDto = {
        name: 'Fluffy',
        breed: 'Persian',
        gender: 'female',
        type: 'cat',
        vaccinated: true,
      };

      const saveSpy = jest.spyOn(animalRepository, 'save').mockResolvedValueOnce(createAnimalDto as any);

      const result = await animalService.saveAnimal(createAnimalDto);

      expect(saveSpy).toHaveBeenCalled();
      expect(result).toEqual(createAnimalDto as any);
    });

    it('should throw an error if save fails', async () => {

      const createAnimalDto: CreateAnimalDto = {
        name: 'Fluffy',
        breed: 'Persian',
        gender: 'female',
        type: 'cat',
        vaccinated: true,
      };

      const saveSpy = jest.spyOn(animalRepository, 'save').mockRejectedValueOnce(new Error('save failed'));

      await expect(animalService.saveAnimal(createAnimalDto)).rejects.toThrow(new Error('save failed'));
      expect(saveSpy).toHaveBeenCalled();
    });
  });
});
