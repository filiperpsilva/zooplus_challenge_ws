import { Test, TestingModule } from '@nestjs/testing';
import { AnimalService } from '../animal.service';
import { Repository } from 'typeorm';
import { Animal } from '../animal.entity';

describe('AnimalService.findAll', () => {
  let animalService: AnimalService;
  let animalRepository: Repository<Animal>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AnimalService,
        {
          provide: 'ANIMAL_REPOSITORY',
          useClass: Repository,
        },
      ],
    }).compile();

    animalService = module.get<AnimalService>(AnimalService);
    animalRepository = module.get<Repository<Animal>>('ANIMAL_REPOSITORY');
  });

  describe('findAll', () => {
    it('should return an array of animals', async () => {
      const animals: any = [
        { id: '1', name: 'Cat', breed: 'Siamese', gender: 'Female', type: 'Mammal', vaccinated: true },
        { id: '2', name: 'Dog', breed: 'Labrador', gender: 'Male', type: 'Mammal', vaccinated: false },
      ];

      jest.spyOn(animalRepository, 'find').mockResolvedValue(animals);

      const result = await animalService.findAll();

      expect(result).toEqual(animals);
    });

    it('should return animals in ascending order by creation time', async () => {
      const animals: any = [
        { id: '1', name: 'Cat', breed: 'Siamese', gender: 'Female', type: 'Mammal', vaccinated: true, createdAt: new Date('2023-04-01T12:00:00.000Z') },
        { id: '2', name: 'Dog', breed: 'Labrador', gender: 'Male', type: 'Mammal', vaccinated: false, createdAt: new Date('2023-04-02T12:00:00.000Z') },
        { id: '3', name: 'Hamster', breed: 'Golden', gender: 'Male', type: 'Mammal', vaccinated: true, createdAt: new Date('2023-04-03T12:00:00.000Z') },
      ];

      jest.spyOn(animalRepository, 'find').mockResolvedValue(animals);

      const result = await animalService.findAll();

      expect(result[0]).toEqual(animals[0]);
      expect(result[1]).toEqual(animals[1]);
      expect(result[2]).toEqual(animals[2]);
    });
  });
});
