import { IsString, IsNotEmpty, IsBoolean } from 'class-validator';

export class CreateAnimalDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  type: string;

  @IsString()
  @IsNotEmpty()
  breed: string;

  @IsString()
  @IsNotEmpty()
  gender: string;

  @IsBoolean()
  vaccinated: boolean;
}

export class UpdateAnimalDto extends CreateAnimalDto {
  @IsString()
  @IsNotEmpty()
  id: string;
}

export class GetAnimalByIdDto {
    @IsString()
    @IsNotEmpty()
    id?: string;
}
