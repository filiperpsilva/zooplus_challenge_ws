import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Animal } from './animal.entity';
import { CreateAnimalDto, UpdateAnimalDto } from './animal.dto';

@Injectable()
export class AnimalService {
  constructor(
    @Inject('ANIMAL_REPOSITORY')
    private animalRepository: Repository<Animal>,
  ) {}

  async findAll(): Promise<Animal[]> {
    return this.animalRepository.find({order: { createdAt: 'ASC' }});
  }

  async findById(animalId: string): Promise<Animal> {
    const animal = await this.animalRepository.findOne({ where: { id: animalId } });
    if (!animal) {
      throw new Error(`Animal with id ${animalId} not found`);
    }
    return animal;
  }
  
  async saveAnimal(data: CreateAnimalDto): Promise<Animal> {
    try {
      const animal = new Animal();
      animal.name = data.name;
      animal.breed = data.breed;
      animal.gender = data.gender;
      animal.type = data.type;
      animal.vaccinated = data.vaccinated;

      return this.animalRepository.save(animal);
    } catch (error) {
      throw new Error(error);
    }
  }

  async updateAnimal(id, data: UpdateAnimalDto): Promise<Animal> {
    try {
      const animal = await this.animalRepository.findOne({ where: {id} });
  
      if (!animal) {
        throw new Error(`Animal with ID ${id} not found`);
      }
  
      animal.name = data.name;
      animal.breed = data.breed;
      animal.gender = data.gender;
      animal.type = data.type;
      animal.vaccinated = data.vaccinated;
  
      const updatedAnimal = await this.animalRepository.save(animal);
  
      return updatedAnimal;
    } catch (error) {
      throw new Error(`Failed to update animal with ID ${id}: ${error.message}`);
    }
  }
}
