import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Animal {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  type: string;

  @Column()
  breed: string;

  @Column()
  gender: string;

  @Column()
  vaccinated: boolean;

  @Column({ nullable: true, default: new Date().toISOString().split('T')[0] })
  lastVisit: Date;

  @Column({ nullable: true })
  lastUpdate: Date;

  @CreateDateColumn()
  createdAt: Date;
}
