import { BadRequestException, Body, Controller, Get, Logger, NotFoundException, Param, ParseUUIDPipe, Post, Put } from '@nestjs/common';
import { Animal } from './animal.entity';
import { AnimalService } from './animal.service';
import { CreateAnimalDto, UpdateAnimalDto } from './animal.dto';

@Controller('/animals')
export class AnimalController {
  private readonly logger = new Logger(AnimalController.name);
  constructor(private readonly animalService: AnimalService) {}

  @Get('/')
  async getAnimals(): Promise<{ animals: Animal[] }> {
    this.logger.log('Retrieving all animals');
    const animals = await this.animalService.findAll();
    return { animals };
  }

  @Get('/:id')
  async getAnimalById(@Param('id', new ParseUUIDPipe()) id: string): Promise<{ animal: Animal }> {
    this.logger.log(`Retrieving animal with ID ${id}`);
    const animal = await this.animalService.findById(id);
    if (!animal) {
      throw new NotFoundException('Animal not found');
    }
    return { animal };
  }

  @Post('/')
  async createAnimal(@Body() data: CreateAnimalDto): Promise<{ animal: Animal }> {
    try {
      this.logger.log('Creating a new animal');
      const animal = await this.animalService.saveAnimal(data);
      return { animal };
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  @Put('/:id')
  async updateAnimal(
    @Param('id') id: string,
    @Body() data: UpdateAnimalDto,
  ): Promise<{ animal: Animal }> {
    try {
      this.logger.log(`Updating animal with ID ${id}`);
      const animal = await this.animalService.updateAnimal(id, data);
      return { animal };
    } catch (error) {
      throw new NotFoundException('Animal not found');
    }
  }
}
