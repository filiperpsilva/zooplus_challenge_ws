import { DataSource } from 'typeorm';
import { Animal } from '../animal/animal.entity';

export const databaseProviders = [
  {
    provide: 'DATA_SOURCE',
    useFactory: async () => {
      const dataSource = new DataSource({
        type: 'postgres',
        host: process.env.POSTGRES_HOST,
        port: 5432,
        username: 'postgres',
        password: process.env.POSTGRES_PASSWORD,
        database: 'zooplus_challenge',
        entities: [
          Animal,
          //__dirname + '/../**/*.entity{.ts,.js}',
        ],
        synchronize: true,
      });
      return dataSource.initialize();
    },
  },
];
