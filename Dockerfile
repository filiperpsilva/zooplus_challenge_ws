FROM node:18.12.1-alpine as build
WORKDIR /app
COPY . .
RUN npm ci 
RUN npm run build
RUN mkdir /app/build && cp -r /app/dist/* /app/build

EXPOSE 80
CMD ["node", "dist/main.js"]